import numpy as np
import codecs
import glob
from nltk.tokenize import RegexpTokenizer
import math
from collections import Counter, OrderedDict
import json
import time

time1 = time.process_time()

encodingFileMode = "utf-16-le"
allFiles = glob.glob("news_dataset/news_dataset/*.txt")
tokenizer = RegexpTokenizer(r'\w+')
stopWordsFile = "stopWordsVietnamese.txt"
stopWords = []
allUniqueWords = set()
numOfDocs = 0
numOfUniqueWords = 0
tfidfDict = []
idfDict = None
ratio = [0.4, 0.2, 0.4]  # title body author
# get stop words
with codecs.open(stopWordsFile, mode="r", encoding="utf-8") as f:
    stopWords.extend(f.read().splitlines())


def cleanSentence(sentences):
    return Counter([i for i in tokenizer.tokenize(
        sentences.lower()) if i not in stopWords])


print("getting docs")
# get words and count not in stopwords for per doc

titleDocs = []
bodyDocs = []
authorDocs = []
allDocs = []


for txt in allFiles:
    with codecs.open(txt, mode="r", encoding=encodingFileMode) as f:
        title = f.readline()
        body = f.read()
        pos = body.rfind(".") + 1
        author = body[pos:]
        body = body[0: pos]
        titleDocs.append(cleanSentence(title))
        bodyDocs.append(cleanSentence(body))
        authorDocs.append(cleanSentence(author))
        allDocs.append(titleDocs[-1] + bodyDocs[-1] + authorDocs[-1])
        allUniqueWords.update(allDocs[-1].keys())

numOfDocs = len(allDocs)

allUniqueWords = sorted(allUniqueWords)
numOfUniqueWords = len(allUniqueWords)

print("finished loading docs")

# write all unique words filtered stop words
with codecs.open("uniqueWords.txt", mode="w+", encoding="utf-8") as f:
    f.write(",".join(allUniqueWords))

print("Start computing idf")

# Calculate idf


def idf_compute():
    idfDict = dict.fromkeys(allUniqueWords, 0)
    for doc in allDocs:
        for word, val in doc.items():
            if val > 0:
                idfDict[word] += 1

    for word, val in idfDict.items():
        idfDict[word] = 1 + math.log2(numOfDocs/val)

    return idfDict


idfDict = idf_compute()
print("Finish compute idf")

with codecs.open("idf.txt", "w+", "utf-8") as f:
    f.write(json.dumps(idfDict))


# sentences have to be cleaned first
def tf_compute(doc):
    for word, val in doc.items():
        doc[word] = 1 + math.log2(val)
    return doc


def tf_idf_compute(doc, ratio=1):
    # doc = cleanSentence(doc)
    # tfDict = tf_compute(doc)
    # tfidfDoc = OrderedDict.fromkeys(allUniqueWords, 0)
    for word, val in doc.items():
        doc[word] = idfDict[word] * (1 + math.log2( doc[word] )) * ratio

    return doc


print("Start computing tfidf")

for i in range(numOfDocs):
    tfidfTitle = tf_idf_compute(titleDocs[i], ratio[0])
    tfidfBody = tf_idf_compute(bodyDocs[i], ratio[1])
    tfidfAuthor = tf_idf_compute(authorDocs[i], ratio[2])
    tfidfDict.append(tfidfTitle + tfidfBody + tfidfAuthor)

print("Finished computing tfidf")

# tfidfString = ""
# for tfidf in tfidfDict:
#     temp = []
#     for word, val in tfidf.items():
#         temp.append(str.format("{0}:{1}", allUniqueWords.index(word), val))
#     tfidfString += ",".join(temp) + "\n"

print("finish convert tfidf to string")
# with open("tfidf.txt", "w+") as f:
#     f.write(tfidfString)

with codecs.open("tfidf.txt", "w+", "utf-8") as f:
    f.write(json.dumps(tfidfDict))

print(time.process_time() - time1)


