from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.conf import settings

# quey parts

import numpy as np
import codecs
import glob
from nltk.tokenize import RegexpTokenizer
import math
from collections import Counter, OrderedDict
import json
import os 
dir_path = os.path.dirname(os.path.realpath(__file__)) + "\\static\\ir\\"
 
threshhold = None
k_minSim = None
encodingFileMode = "utf-16-le"
allFiles = glob.glob(dir_path + "news_dataset\\news_dataset\\*.txt")
tokenizer = RegexpTokenizer(r'\w+')
stopWordsFile = dir_path + "stopWordsVietnamese.txt"
stopWords = []
allUniqueWords = set()
numOfDocs = 0
numOfUniqueWords = 0
tfidfDict = []
idfDict = None
ratio = [0.4, 0.2, 0.4]  # title body author

# get stop words
with codecs.open(stopWordsFile, mode="r", encoding="utf-8") as f:
    stopWords.extend(f.read().splitlines())

def cleanSentence(sentences):
    return Counter([i for i in tokenizer.tokenize(
        sentences.lower()) if i not in stopWords])

with codecs.open(dir_path + "idf.txt", "r", encoding="utf-8") as f:
    idfDict = json.loads(f.read())

with codecs.open(dir_path + "tfidf.txt", "r", encoding="utf-8") as f:
    tfidfDict = json.loads(f.read())

numOfDocs = len(tfidfDict)
numOfUniqueWords = len(idfDict)

# sentences have to be cleaned first
def tf_compute(doc):
    for word, val in doc.items():
        doc[word] = 1 + math.log2(val)
    return doc

def tf_idf_compute(doc, ratio=1):
    for word, val in doc.items():
        try:
            doc[word] = idfDict[word] * (1 + math.log2( doc[word] )) * ratio
        except:
            doc[word] = 0 
    return doc

def cosine_sim_dicts(a, b):
    cos_sim = sum(a[key]*b.get(key, 0) for key in a)/(np.linalg.norm(tuple(a.values()))*np.linalg.norm(tuple(b.values())))
    return cos_sim

def get_sim_sorted(simList, k):
    return np.argpartition(simList, k)

# Create your views here.
def index(request):
    template = 'index.html'
    return render(request, template)

def query(request):
    query = request.GET["query"]
    tfidfQ = tf_idf_compute( cleanSentence(query))
    simList = []
    for i in range(numOfDocs):
        simList.append(cosine_sim_dicts(tfidfQ, tfidfDict[i]))
  
    pos = np.where( np.array(simList) > 0 )

    #  = get_sim_sorted(simList, k_minSim).arg

    
    result = []
    # for i in pos:
        # result.append(allFiles[i])
    if len(pos) == 0:
        return HttpResponse("No result")
    else:
        # return HttpResponse(json.dumps([allFiles[i].split('\\')[-1] for i in pos]))
        return JsonResponse([allFiles[i].split("\\")[-1] for i in pos[0]], safe=False)

def relevance(request):
    with codecs.open(dir_path + "benchmark\\" + request.GET['query']+".txt", "w+", "utf-8") as f:
        f.write(json.dumps(request.GET.getlist('relevance[]')))
    return HttpResponse("ok")
