from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('ir', views.query, name='query'),
    path('relevance', views.relevance, name='relevance'),
]
