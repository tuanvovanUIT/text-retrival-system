import numpy as np
import codecs
import glob
from nltk.tokenize import RegexpTokenizer
import math
from collections import Counter, OrderedDict
import json

k_minSim = 5

encodingFileMode = "utf-16-le"

allFiles = glob.glob("news_dataset/news_dataset/*.txt")
tokenizer = RegexpTokenizer(r'\w+')
stopWordsFile = "stopWordsVietnamese.txt"
stopWords = []
allUniqueWords = set()
numOfDocs = 0
numOfUniqueWords = 0
tfidfDict = []
idfDict = None
ratio = [0.4, 0.2, 0.4]  # title body author

# get stop words
with codecs.open(stopWordsFile, mode="r", encoding="utf-8") as f:
    stopWords.extend(f.read().splitlines())


def cleanSentence(sentences):
    return Counter([i for i in tokenizer.tokenize(
        sentences.lower()) if i not in stopWords])

with codecs.open("idf.txt", "r", encoding="utf-8") as f:
    idfDict = json.loads(f.read())

with codecs.open("tfidf.txt", "r", encoding="utf-8") as f:
    tfidfDict = json.loads(f.read())

numOfDocs = len(tfidfDict)
numOfUniqueWords = len(idfDict)

# sentences have to be cleaned first
def tf_compute(doc):
    for word, val in doc.items():
        doc[word] = 1 + math.log2(val)
    return doc


def tf_idf_compute(doc, ratio=1):
    for word, val in doc.items():
        doc[word] = idfDict[word] * (1 + math.log2( doc[word] )) * ratio
    return doc

def cosine_sim_dicts(a, b):
    cos_sim = sum(a[key]*b.get(key, 0) for key in a)/(np.linalg.norm(tuple(a.values()))*np.linalg.norm(tuple(b.values())))
    return cos_sim

def get_sim_sorted(simList, k):
    return np.argpartition(simList, k)[::-1]

query = [
    "việt nam máy tính"
]

import time

time1 = time.process_time()

simList = []
tfidfQ = tf_idf_compute( cleanSentence(query[0]))

for i in range(numOfDocs):
    simList.append(cosine_sim_dicts(tfidfQ, tfidfDict[i]))



simList = np.array(simList)

maxK = get_sim_sorted(simList, k_minSim)[0:k_minSim]

 
for i in maxK:
    print(allFiles[i])

print(  time.process_time() - time1)

